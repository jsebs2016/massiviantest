package com.test.massivian.dto;

import lombok.Data;

@Data
public class UserDTO {
	private String idUser;
	private String nameUser;
	private String moneyUser;
	private String userState;
}

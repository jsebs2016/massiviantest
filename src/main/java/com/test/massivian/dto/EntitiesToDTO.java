package com.test.massivian.dto;

import com.test.massivian.constant.CodeState;
import com.test.massivian.constant.MessageStatus;
import com.test.massivian.model.BetRoulette;
import com.test.massivian.model.Roulette;
import com.test.massivian.model.User;

public class EntitiesToDTO {

	private EntitiesToDTO() {

	}

	public static MessageDTO sendResponseRoulette(Roulette roulette) {
		if (null != roulette) {
			return new MessageDTO(CodeState.CODE_SUCESSFUL.getCode(),
					MessageStatus.MESSAGE_SUCESSFUL.getMessage() + roulette.getIdRoulette());
		}
		return new MessageDTO(CodeState.CODE_FAILED.getCode(), MessageStatus.MESSAGE_FAILED.getMessage());

	}

	public static MessageDTO sendResponseRouletteOpening(Roulette roulette) {
		if (null != roulette) {
			return new MessageDTO(CodeState.CODE_SUCESSFUL.getCode(),
					MessageStatus.MESSAGE_ROULETTE_OPENING_SUCESSFUL.getMessage() + roulette.getIdRoulette());
		}
		return new MessageDTO(CodeState.CODE_FAILED.getCode(),
				MessageStatus.MESSAGE_ROULETT_OPENING_FAILED.getMessage());
	}

	public static MessageDTO sendResponseUser(User user) {
		if (null != user) {
			return new MessageDTO(CodeState.CODE_SUCESSFUL.getCode(), MessageStatus.MESSAGE_USER_SUCESSFUL.getMessage()
					+ user.getUserState() + " User id: " + user.getIdUser());
		}
		return new MessageDTO(CodeState.CODE_FAILED.getCode(), MessageStatus.MESSAGE_FAILED.getMessage());
	}

	public static MessageDTO sendResponseBetRoulette(BetRoulette betRoulette) {
		if (null != betRoulette) {
			return new MessageDTO(CodeState.CODE_SUCESSFUL.getCode(),
					MessageStatus.MESSAGE_BET_ROULETTE_SUCESSFUL.getMessage() + betRoulette.getIdRoulette());
		}
		return new MessageDTO(CodeState.CODE_FAILED.getCode(), MessageStatus.MESSAGE_FAILED.getMessage());
	}
}

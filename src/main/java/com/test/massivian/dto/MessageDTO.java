package com.test.massivian.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MessageDTO {

	private String code;
	private String messageBody;
}

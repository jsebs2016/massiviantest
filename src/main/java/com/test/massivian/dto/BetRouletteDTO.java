package com.test.massivian.dto;

import lombok.Data;

@Data
public class BetRouletteDTO {

	private String idBetRoulette;
	private String idRoulette;
	private String number;
	private String color;
	private String betMoney;
	private String idUser;
}

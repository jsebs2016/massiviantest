package com.test.massivian.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.test.massivian.constant.Key;
import com.test.massivian.constant.MessageStatus;
import com.test.massivian.dto.UserDTO;
import com.test.massivian.exception.TestException;
import com.test.massivian.model.User;
import com.test.massivian.respository.RepositoryInterface;

import lombok.extern.slf4j.Slf4j;

import com.test.massivian.constant.UserState;

@Repository
@Slf4j
public class UserService implements RepositoryInterface<User, UserDTO> {

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	private HashOperations<String, String, User> hashOperations;

	@Override
	public void saveInDatabase(String hashKey, String value, User object) throws TestException {
		hashOperations = redisTemplate.opsForHash();
		hashOperations.put(hashKey, value, object);
	}

	@Override
	public User addDtoObjects(UserDTO userDto) throws TestException {
		User user = new User();
		try {
			user.setIdUser(userDto.getIdUser());
			user.setMoneyUser(userDto.getMoneyUser());
			user.setNameUser(userDto.getNameUser());
			Boolean flag = evaluateMoney(Integer.parseInt(user.getMoneyUser()));
			if(flag.booleanValue()) {
				user.setUserState(UserState.USER_STATE_SUCESSFUL.getUserState());
			} else {
				user.setUserState(UserState.USER_STATE_FAILED.getUserState());
			}
			saveInDatabase(Key.USER_KEY.getHashKey(), user.getIdUser(), user);
		} catch (Exception e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
			throw new TestException(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}

		return user;
	}

	private User getUserById(String hashKey, String value) {
		hashOperations = redisTemplate.opsForHash();
		return hashOperations.get(hashKey, value);
	}

	public User getUser(String idUser) throws TestException {
		User user = new User();
		try {
			user = getUserById(Key.USER_KEY.getHashKey(), idUser);
		} catch (Exception e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
			throw new TestException(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}

		return user;
	}

	public Boolean evaluateAllowedUser(User user) {
		return user.getUserState().equals(UserState.USER_STATE_SUCESSFUL.getUserState());
	}

	public Boolean evaluateMoney(Integer number) {
		return number >= 1 && number <= 10000;
	}

}

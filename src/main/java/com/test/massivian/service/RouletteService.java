package com.test.massivian.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.test.massivian.dto.RouletteDTO;
import com.test.massivian.exception.TestException;
import com.test.massivian.model.Roulette;
import com.test.massivian.respository.RepositoryInterface;

import lombok.extern.slf4j.Slf4j;

import com.test.massivian.constant.Key;
import com.test.massivian.constant.MessageStatus;
import com.test.massivian.constant.RouletteState;

@Repository
@Slf4j
public class RouletteService implements RepositoryInterface<Roulette, RouletteDTO> {

	private HashOperations<String, String, Roulette> hashOperations;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@Override
	public void saveInDatabase(String hashKey, String value, Roulette object) throws TestException {
		hashOperations = redisTemplate.opsForHash();
		hashOperations.put(hashKey, value, object);
	}

	@Override
	public Roulette addDtoObjects(RouletteDTO rouletteDTO) throws TestException {
		Roulette roulette = new Roulette();
		try {
			roulette.setIdRoulette(rouletteDTO.getIdRoulette());
			roulette.setRouletteState(RouletteState.ROULETTE_STATE_CREATED.getStateRoulette());
			roulette.setOpeningDate(generateDate());
			roulette.setClosingDate("Aun no se han cerrado las apuestas");
			saveInDatabase(Key.ROULETTE_KEY.getHashKey(), roulette.getIdRoulette(), roulette);
		} catch (Exception e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
			throw new TestException(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}

		return roulette;
	}

	public Roulette updateByIdRoulette(String idRoulette, String rouletteState) throws TestException {
		Roulette roulette = new Roulette();
		try {
			roulette = hashOperations.get(Key.ROULETTE_KEY.getHashKey(), idRoulette);
			roulette.setRouletteState(rouletteState);
			if (rouletteState.equals(RouletteState.ROULETTE_STATE_CLOSED.getStateRoulette())) {
				roulette.setClosingDate(generateDate());
			}
			saveInDatabase(Key.ROULETTE_KEY.getHashKey(), roulette.getIdRoulette(), roulette);
		} catch (Exception e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
			throw new TestException(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}

		return roulette;
	}

	private List<Roulette> getRoulettes(String hashKey) {
		hashOperations = redisTemplate.opsForHash();
		return hashOperations.values(hashKey);
	}

	public List<Roulette> getAllRoulette() throws TestException {
		List<Roulette> rouletteList = new ArrayList<>();
		try {
			rouletteList = getRoulettes(Key.ROULETTE_KEY.getHashKey());
		} catch (Exception e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
			throw new TestException(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}
		return rouletteList;
	}

	private String generateDate() {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		return dateFormat.format(date);
	}

}

package com.test.massivian.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.test.massivian.constant.BetRouletteState;
import com.test.massivian.constant.Key;
import com.test.massivian.constant.MessageStatus;
import com.test.massivian.dto.BetRouletteDTO;
import com.test.massivian.exception.TestException;
import com.test.massivian.model.BetRoulette;
import com.test.massivian.respository.RepositoryInterface;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class BetRouletteService implements RepositoryInterface<BetRoulette, BetRouletteDTO> {

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	private HashOperations<String, String, BetRoulette> hashOperations;

	@Override
	public void saveInDatabase(String hashKey, String value, BetRoulette object) throws TestException {
		hashOperations = redisTemplate.opsForHash();
		hashOperations.put(hashKey, value, object);
	}

	@Override
	public BetRoulette addDtoObjects(BetRouletteDTO betRouletteDTO) throws TestException {
		BetRoulette betRouletteTemp = new BetRoulette();
		try {
			BetRoulette betRoulette = new BetRoulette();
			Boolean color = evaluateColor(betRouletteDTO.getColor());
			Boolean number = evaluateNumbers(Integer.parseInt(betRouletteDTO.getNumber()));
			if (color.booleanValue() && number.booleanValue()) {

				betRoulette.setColor(betRouletteDTO.getColor());
				betRoulette.setIdRoulette(betRouletteDTO.getIdRoulette());
				betRoulette.setIdBetRoulette(betRouletteDTO.getIdBetRoulette());
				betRoulette.setNumber(betRouletteDTO.getNumber());
				betRoulette.setIdUser(betRouletteDTO.getIdUser());
				betRoulette.setBetMoney(betRouletteDTO.getBetMoney());
				betRoulette.setBetRouletteState(BetRouletteState.BET_ROULETTE_OPEN.getBetRouletteState());
				saveInDatabase(Key.BET_ROULETTE_KEY.getHashKey(), betRoulette.getIdBetRoulette(), betRoulette);
				betRouletteTemp = betRoulette;
			} else {
				betRouletteTemp = null;
			}
		} catch (Exception e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
			throw new TestException(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}
		return betRouletteTemp;

	}

	public void updateByIdRoulette(BetRouletteDTO betRouletteDTO) throws TestException {
		try {
			List<BetRoulette> listBetRoulette = hashOperations.values(Key.BET_ROULETTE_KEY.getHashKey());
			for (BetRoulette betRoulette : listBetRoulette) {
				if (betRoulette.getIdRoulette().equals(betRouletteDTO.getIdRoulette())) {
					Boolean flag = getIdBetRoulette(betRoulette.getIdBetRoulette());
					if(flag.booleanValue()) {
						betRoulette.setBetRouletteState(BetRouletteState.BET_ROULETTE_CLOSE.getBetRouletteState());
						saveInDatabase(Key.BET_ROULETTE_KEY.getHashKey(), betRoulette.getIdBetRoulette(), betRoulette);
					}
				}
			}
		} catch (Exception e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
			throw new TestException(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}
	}
	
	private Boolean getIdBetRoulette(String idBetRoulette) {
		hashOperations = redisTemplate.opsForHash();
		BetRoulette betRoulette = hashOperations.get(Key.BET_ROULETTE_KEY.getHashKey(), idBetRoulette);
		return idBetRoulette.equals(betRoulette.getIdBetRoulette());
	}

	private List<BetRoulette> getRoulettes(String hashKey) {
		hashOperations = redisTemplate.opsForHash();
		return hashOperations.values(hashKey);
	}

	public List<BetRoulette> getAllBetRoulette() throws TestException {
		List<BetRoulette> listBetRoulette = new ArrayList<>();
		try {
			listBetRoulette = getRoulettes(Key.BET_ROULETTE_KEY.getHashKey());
		} catch (Exception e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
			throw new TestException(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}

		return listBetRoulette;
	}

	public Boolean evaluateColor(String color) {
		return color.equals("ROJO") || color.equals("NEGRO");
	}

	public Boolean evaluateNumbers(Integer number) {
		return number >= 0 && number <= 36;
	}

}

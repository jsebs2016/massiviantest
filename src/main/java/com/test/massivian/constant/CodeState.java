package com.test.massivian.constant;

public enum CodeState {
	
	CODE_SUCESSFUL("200"),
	CODE_FAILED("500");
	
	private final String code;

	private CodeState(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
	
	
	
}

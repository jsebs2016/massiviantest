package com.test.massivian.constant;

public enum MessageStatus {

	MESSAGE_BET_ROULETTE_SUCESSFUL("Apuesta añadida con exito en la ruleta: "),
	MESSAGE_USER_SUCESSFUL("Participante almacenado. Estado participante : "),
	MESSAGE_ROULETTE_OPENING_SUCESSFUL("Operacion exitosa. Ruleta abierta: "),
	MESSAGE_ROULETT_OPENING_FAILED("Operacion denegada"), 
	MESSAGE_FAILED("Operacion fallida"),
	MESSAGE_SUCESSFUL("Operacion exitosa. Roulette Transaction: ");

	private final String message;

	private MessageStatus(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}

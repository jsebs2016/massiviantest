package com.test.massivian.constant;

public enum RouletteState {

	ROULETTE_STATE_CREATED("CREADA"),
	ROULETTE_STATE_CLOSED("CERRADA"),
	ROULETTE_STATE_OPEN("ABIERTA");
	
	private final String stateRoulette;

	private RouletteState(String stateRoulette) {
		this.stateRoulette = stateRoulette;
	}

	public String getStateRoulette() {
		return stateRoulette;
	}
	
	
}

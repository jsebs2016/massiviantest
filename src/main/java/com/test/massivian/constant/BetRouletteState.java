package com.test.massivian.constant;

public enum BetRouletteState {

	BET_ROULETTE_OPEN("APUESTA_ABIERTA"),
	BET_ROULETTE_CLOSE("APUESTA_CERRADA");
	
	private final String betRouletteState;

	private BetRouletteState(String betRouletteState) {
		this.betRouletteState = betRouletteState;
	}

	public String getBetRouletteState() {
		return betRouletteState;
	}
	
	
}

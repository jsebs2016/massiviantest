package com.test.massivian.constant;

public enum UserState {

	USER_STATE_SUCESSFUL("USUARIO_PERMITIDO"),
	USER_STATE_FAILED("USUARIO_DENEGADO");
	
	private final String userState;

	private UserState(String userState) {
		this.userState = userState;
	}

	public String getUserState() {
		return userState;
	}
	
	
}

package com.test.massivian.constant;

public enum Key {
	
	BET_ROULETTE_KEY("BETROULETTE"),
	USER_KEY("USER"),
	ROULETTE_KEY("ROULETTE");
	
	private final String hashKey;

	private Key(String hashKey) {
		this.hashKey = hashKey;
	}

	public String getHashKey() {
		return hashKey;
	}
	
	
}

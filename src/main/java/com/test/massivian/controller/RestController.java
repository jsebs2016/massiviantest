package com.test.massivian.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import com.test.massivian.constant.MessageStatus;
import com.test.massivian.constant.RouletteState;
import com.test.massivian.dto.BetRouletteDTO;
import com.test.massivian.dto.EntitiesToDTO;
import com.test.massivian.dto.MessageDTO;
import com.test.massivian.dto.RouletteDTO;
import com.test.massivian.dto.UserDTO;
import com.test.massivian.exception.TestException;
import com.test.massivian.model.BetRoulette;
import com.test.massivian.model.Roulette;
import com.test.massivian.model.User;
import com.test.massivian.service.BetRouletteService;
import com.test.massivian.service.RouletteService;
import com.test.massivian.service.UserService;

import lombok.extern.slf4j.Slf4j;

@org.springframework.web.bind.annotation.RestController
@RequestMapping(value = "/api")
@Slf4j
public class RestController {
	
	@Autowired
	private RouletteService rouletteService;
	@Autowired
	private UserService userService;
	@Autowired
	private BetRouletteService betRouletteService;
	
	@PostMapping("/AgregarRuleta")
	public MessageDTO addRoulette(@RequestBody RouletteDTO rouletteDTO)  {
		Roulette roulette;
		try {
			roulette = rouletteService.addDtoObjects(rouletteDTO);
		} catch (TestException e) {
			roulette = null;
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}
		return EntitiesToDTO.sendResponseRoulette(roulette);
	}
	
	@PostMapping("/AgregarParticipante")
	public MessageDTO addUser(@RequestBody UserDTO userDTO) {
		User user;
		try {
			user = userService.addDtoObjects(userDTO);
		} catch (TestException e) {
			user = null;
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}
		return EntitiesToDTO.sendResponseUser(user);
	}
	
	@GetMapping("/verApuestas")
	public List<BetRoulette> getAllBetRoulette(){
		try {
			return betRouletteService.getAllBetRoulette();
		} catch (TestException e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}
		return Collections.emptyList();
	}
	
	@PostMapping("/AgregarApuesta")
	public MessageDTO addBetRoulette(@RequestHeader(name = "USUARIO", required = true) String idUser,
			@RequestBody BetRouletteDTO betRouletteDTO) {
		BetRoulette betRoulette = new BetRoulette();
		User user;
		try {
			user = userService.getUser(idUser);
			Boolean flag = userService.evaluateAllowedUser(user);
			if(flag.booleanValue()) {
				betRouletteDTO.setIdUser(idUser);
				betRoulette = betRouletteService.addDtoObjects(betRouletteDTO);
			} else {
				betRoulette = null;
			}
		} catch (TestException e) {
			betRoulette = null;
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}
		
		return EntitiesToDTO.sendResponseBetRoulette(betRoulette);
	}
	
	@PostMapping("/CerrarApuesta")
	public List<BetRoulette> closeBetRoulette(@RequestBody BetRouletteDTO betRouletteDTO) {
		try {
			betRouletteService.updateByIdRoulette(betRouletteDTO);
			rouletteService.updateByIdRoulette(betRouletteDTO.getIdRoulette(),RouletteState.ROULETTE_STATE_CLOSED.getStateRoulette());
			return betRouletteService.getAllBetRoulette();
		} catch (TestException e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}
		return Collections.emptyList();
	}
	
	@GetMapping("/AperturaRuleta/{idRoulette}")
	public MessageDTO getRouletteById(@PathVariable String idRoulette) {
		
		Roulette roulette;
		try {
			roulette = rouletteService.updateByIdRoulette(idRoulette,RouletteState.ROULETTE_STATE_OPEN.getStateRoulette());
		} catch (TestException e) {
			roulette = null;
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}
		return EntitiesToDTO.sendResponseRouletteOpening(roulette);
	}
	
	@GetMapping("/ListarRuletas")
	public List<Roulette> getAllRoulettes() {
		try {
			return rouletteService.getAllRoulette();
		} catch (TestException e) {
			log.error(MessageStatus.MESSAGE_FAILED.getMessage(), e);
		}
		return Collections.emptyList();
	}

}

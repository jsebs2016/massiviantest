package com.test.massivian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.test.massivian"})
public class MassivianTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MassivianTestApplication.class, args);
	}

}

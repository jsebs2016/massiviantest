package com.test.massivian.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import lombok.Data;

@Data
@RedisHash("ROULETTE")
public class Roulette implements Serializable{

	private static final long serialVersionUID = -8109731107000448847L;
	@Id
	private String idRoulette;
	private String rouletteState;
	private String openingDate;
	private String closingDate;
}

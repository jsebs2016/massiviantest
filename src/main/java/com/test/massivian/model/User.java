package com.test.massivian.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import lombok.Data;

@Data
@RedisHash("USER")
public class User implements Serializable{
	
	
	private static final long serialVersionUID = 5538522468434871687L;
	@Id
	private String idUser;
	private String nameUser;
	private String moneyUser;
	private String userState;

}

package com.test.massivian.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import lombok.Data;

@Data
@RedisHash("BETROULETTE")
public class BetRoulette implements Serializable {

	private static final long serialVersionUID = -1936175679864081004L;
	@Id
	private String idBetRoulette;
	private String idRoulette;
	private String number;
	private String color;
	private String betMoney;
	private String idUser;
	private String betRouletteState;
	
}

package com.test.massivian.respository;

import com.test.massivian.exception.TestException;

public interface RepositoryInterface <T,R> {

	 T addDtoObjects(R dto) throws TestException;
	 void saveInDatabase(String hashKey, String value, T object) throws TestException;
}

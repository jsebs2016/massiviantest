package com.test.massivian.exception;

public class TestException extends Exception {

	private static final long serialVersionUID = 1124368761599540029L;

	public TestException(String message, Throwable cause) {
		super(message, cause);
	}

	public TestException(String message) {
		super(message);
	}

	public TestException(Throwable cause) {
		super(cause);
	}

	
}
